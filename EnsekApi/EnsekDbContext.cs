﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EnsekApi
{
    public class EnsekDbContext : DbContext
    {
        private string getDatabaseLocation()
        {
            var currentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var dbFileName = "database.db";
            return Path.Combine(currentPath, @"..\..\..\", dbFileName);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=" + getDatabaseLocation());

        public DbSet<MeterReading> MeterReading { get; set; }
    }
}
