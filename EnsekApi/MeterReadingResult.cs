﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsekApi
{
    public class MeterReadingResult
    {
        public int SuccessfulReadings { get; set; }
        public int FailedReadings { get; set; }

    }
}
