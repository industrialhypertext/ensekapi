﻿using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Text.RegularExpressions;

namespace EnsekApi
{
    public class MeterReading
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Ignore]
        public int Id { get; set; }
        public int AccountId { get; set; }
        public DateTime MeterReadingDateTime { get; set; }
        public string MeterReadValue { get; set; }

        public static List<MeterReading> ParseCsvFile(Stream meterReadings)
        {
            using var reader = new StreamReader(meterReadings);
            var csv = new CsvReader(reader, System.Globalization.CultureInfo.CurrentCulture);
            return new List<MeterReading>(csv.GetRecords<MeterReading>());
        }

        public bool Validate()
        {
            // Reading values should be in the format NNNNN
            if ( !Regex.IsMatch(MeterReadValue.Trim(), @"^\d+$") )
            {
                return false;
            }

            // query database for the AccountId and the MeterReadingDateTime

            // You should not be able to load the same entry twice

            // A meter reading must be associated with an Account ID to be deemed valid

            return true;
        }

        public void Save(EnsekDbContext dbContext)
        {
            dbContext.MeterReading.Add(this);
            dbContext.SaveChanges();
        }
    }
}