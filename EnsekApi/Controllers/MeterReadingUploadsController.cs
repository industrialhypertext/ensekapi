﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EnsekApi.Controllers
{
    [ApiController]
    [Route("meter-reading-uploads")]
    public class MeterReadingUploadsController : ControllerBase
    {
        private readonly ILogger<MeterReadingUploadsController> _logger;

        public MeterReadingUploadsController(ILogger<MeterReadingUploadsController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public MeterReadingResult Post(IFormFile meterReadingsCsvFile)
        {
            List<MeterReading> meterReadings = null;

            if (meterReadingsCsvFile != null)
            {
                using var stream = meterReadingsCsvFile.OpenReadStream();
                meterReadings = MeterReading.ParseCsvFile(stream);
            }

            var meterReadingResult = new MeterReadingResult();

            if (meterReadings != null)
            {
                var dbContext = new EnsekDbContext();

                foreach (var meterReading in meterReadings)
                {
                    if (meterReading.Validate())
                    {
                        meterReading.Save(dbContext);
                        meterReadingResult.SuccessfulReadings++;
                    }
                    else
                    {
                        meterReadingResult.FailedReadings++;
                    }
                }
            }

            return meterReadingResult;
        }
    }
}
